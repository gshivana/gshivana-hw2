#!/usr/bin/python                                                                            
                                                                                             
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel


def aggNet():
	CONTROLLER_IP="127.0.0.1"

	net = Mininet( topo=None,build=False)
	net.addController( 'c0', controller=RemoteController, ip='127.0.0.1', port=6633 )

	for h in range(3):
            host = net.addHost('h%s' % (h + 1))
            net.addLink(host, switch)	

	net.start()
	CLI( net )
	net.stop()

if __name__ == '__main__':
	setLogLevel( 'info' )
	aggNet()



